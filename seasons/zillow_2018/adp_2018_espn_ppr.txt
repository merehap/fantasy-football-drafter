RANK	PLAYER, TEAM	POS	AVG PICK	7 DAY +/-	AVG VALUE	7 DAY +/-	%OWN
1	Le'Veon Bell, Pit	RB	1.6	+0.4	62.3	+0.4	100.0
2	Todd Gurley II, LAR	RB	2.1	+0.1	62.2	-0.2	100.0
3	David Johnson, Ari	RB	3.7	+0.5	57.3	+0	100.0
4	Ezekiel Elliott, Dal	RB	4.3	+0.2	58.3	-0.5	100.0
5	Antonio Brown, Pit	WR	5.1	+0.1	59.4	+1.6	100.0
6	Saquon Barkley, NYG	RB	6.7	+1.1	57.3	+1.8	99.9
7	Alvin Kamara, NO	RB	7.2	+0.2	47.8	-1.9	100.0
8	Julio Jones, Atl	WR	9.0	+0.6	51.6	+1.5	99.9
9	DeAndre Hopkins, Hou	WR	9.9	+0.6	49.6	+0.7	100.0
10	Odell Beckham Jr., NYG	WR	10.7	+1	50.5	+2.2	99.9
11	Kareem Hunt, KC	RB	11.1	+0.3	47.1	+0.9	99.9
12	Leonard Fournette, Jax	RB	12.8	-0.5	49.2	+1.3	99.9
13	Dalvin Cook, Min	RB	13.1	+0.5	44.5	+0.1	99.9
14	Keenan Allen, LAC	WR	15.3	+1.6	41.6	+0.9	99.9
15	Melvin Gordon, LAC	RB	15.3	+0.5	44.2	+0.4	99.9
16	Michael Thomas, NO	WR	17.3	+0.7	40.1	+0.4	99.9
17	Christian McCaffrey, Car	RB	17.5	+0.6	42.8	+3.2	99.9
18	A.J. Green, Cin	WR	18.8	+1	40.2	+1.6	99.9
19	Devonta Freeman, Atl	RB	19.5	+0.3	38.2	+1.3	99.8
20	Rob Gronkowski, NE	TE	20.4	-0.4	37.3	+1.6	99.9
21	Davante Adams, GB	WR	21.1	+0.9	36.1	+0.8	99.9
22	Mike Evans, TB	WR	24.4	+1.7	33.2	+0.9	99.8
23	LeSean McCoy, Buf	RB	24.7	+0.7	32.4	+0.1	99.7
24	Travis Kelce, KC	TE	25.7	+1.1	30.0	+1	99.9
25	Jordan Howard, Chi	RB	26.5	+4.5	29.1	+2.5	99.2
26	Joe Mixon, Cin	RB	27.6	+3.4	29.1	+1.4	99.0
27	T.Y. Hilton, Ind	WR	29.4	+1.7	28.1	+0.9	99.7
28	Larry Fitzgerald, Ari	WR	29.7	+3.2	25.2	-0.3	99.7
29	Stefon Diggs, Min	WR	31.2	+0.7	26.4	+0.9	99.7
30	Tyreek Hill, KC	WR	32.1	-0.7	26.1	+1.1	99.8
31	Adam Thielen, Min	WR	32.2	-0.5	24.6	-0.8	99.6
32	Zach Ertz, Phi	TE	33.5	+1.1	22.3	-0.2	99.9
33	Kenyan Drake, Mia	RB	33.7	+10.1	18.7	+2	98.1
34	Aaron Rodgers, GB	QB	34.1	-6.4	24.6	+2.4	99.9
35	Amari Cooper, Oak	WR	38.4	+2.5	22.1	+2.2	99.0
36	Demaryius Thomas, Den	WR	38.5	+3.5	20.2	+0.2	99.0
37	Royce Freeman, Den	RB	38.7	+5.9	19.4	+2.3	98.1
38	Tom Brady, NE	QB	39.7	-7.7	18.3	+1.5	99.4
39	Doug Baldwin, Sea	WR	40.2	+1.6	20.9	+0	98.6
40	Allen Robinson, Chi	WR	41.5	+4.5	18.0	-0.4	98.5
41	Jarvis Landry, Cle	WR	43.0	+1.1	18.4	+1.3	98.9
42	Derrick Henry, Ten	RB	43.9	+1.1	16.7	+0.5	98.1
43	Golden Tate, Det	WR	44.2	+3.2	16.9	+0.1	98.2
44	Josh Gordon, Cle	WR	46.1	+3.1	16.7	-0.6	97.6
45	Alex Collins, Bal	RB	46.1	+6.3	15.7	+1.7	97.4
46	Brandin Cooks, LAR	WR	47.2	+3.6	16.0	+0.6	98.2
47	Marvin Jones Jr., Det	WR	48.8	+4.2	13.7	+0	97.6
48	Chris Hogan, NE	WR	49.4	+9.6	13.0	+3.6	96.9
49	Jay Ajayi, Phi	RB	50.1	+5	14.0	+0.3	97.3
50	JuJu Smith-Schuster, Pit	WR	50.3	+1.8	14.1	+0.2	98.0
51	Marshawn Lynch, Oak	RB	51.0	+3.6	12.4	+0.9	97.1
52	Greg Olsen, Car	TE	52.2	-1	10.0	+0.8	99.6
53	Lamar Miller, Hou	RB	53.2	+4.6	13.9	+2.6	96.3
54	Cam Newton, Car	QB	57.3	-6.7	9.7	+1.4	99.6
55	Dion Lewis, Ten	RB	57.3	+7.3	9.4	+0.9	95.2
56	Russell Wilson, Sea	QB	59.0	-4.1	9.3	+1.2	98.8
57	Jimmy Graham, GB	TE	59.1	-4.7	10.3	+0.9	98.4
58	Deshaun Watson, Hou	QB	59.3	-7.5	11.6	+0.7	98.8
59	Mark Ingram II, NO	RB	60.0	+4.7	11.5	+1.2	95.2
60	Delanie Walker, Ten	TE	60.4	+4.6	6.5	+0	97.8
61	Evan Engram, NYG	TE	63.1	+4.2	6.5	-0.3	97.3
62	Michael Crabtree, Bal	WR	63.3	+6	7.9	+0.4	95.0
63	Alfred Morris, SF	RB	66.0	+104	7.3	+7.3	62.3
64	Emmanuel Sanders, Den	WR	66.1	+6.2	8.5	+0.8	94.6
65	Rex Burkhead, NE	RB	66.2	+14.7	6.4	+1.7	88.6
66	Robert Woods, LAR	WR	66.9	+4.2	7.3	-0.3	94.2
67	Corey Davis, Ten	WR	68.6	+11.5	6.5	+0.2	93.3
68	Marquise Goodwin, SF	WR	70.6	+3.3	7.6	+1.8	93.4
69	Tevin Coleman, Atl	RB	72.2	+6.4	6.4	+1.4	92.9
70	Duke Johnson Jr., Cle	RB	73.4	+5.5	5.4	+0.1	90.4
71	Carson Wentz, Phi	QB	74.6	-9.1	6.5	-0.4	98.8
72	Sammy Watkins, KC	WR	75.4	+5.3	5.7	+0.1	94.0
73	Devin Funchess, Car	WR	77.1	+6.7	5.2	+0.6	93.2
74	Carlos Hyde, Cle	RB	77.6	+1.8	6.5	+2.9	90.3
75	Chris Thompson, Wsh	RB	78.7	+19	4.0	+1	86.0
76	Kyle Rudolph, Min	TE	82.0	-5.4	4.8	+0.3	96.4
77	Alshon Jeffery, Phi	WR	83.2	-20.1	10.0	-4	94.3
78	Robby Anderson, NYJ	WR	83.2	+12.8	3.6	+0.5	88.9
79	Chris Carson, Sea	RB	83.6	+10.5	4.8	+1.4	86.0
80	Jordan Reed, Wsh	TE	84.2	+2.4	4.0	+0.3	94.4
81	Pierre Garcon, SF	WR	84.2	+5.8	3.5	-0.4	90.2
82	Andrew Luck, Ind	QB	86.0	-4.8	5.1	+0.4	95.0
83	Will Fuller V, Hou	WR	87.8	+4.7	4.3	-0.4	91.0
84	Julian Edelman, NE	WR	89.4	+4.4	5.2	+0.9	89.8
85	Cooper Kupp, LAR	WR	89.6	+5.8	3.5	+0.5	89.8
86	Jaguars D/ST	D/ST	91.7	-16.8	5.9	-0.4	99.8
87	Drew Brees, NO	QB	92.3	-15.8	8.3	+1.4	95.2
88	Peyton Barber, TB	RB	93.6	+24.2	2.9	+0.3	72.9
89	Sterling Shepard, NYG	WR	93.7	+11	2.7	+0.3	85.1
90	Ben Roethlisberger, Pit	QB	95.0	-5.3	4.0	+0.8	94.2
91	Jamison Crowder, Wsh	WR	95.0	+12.2	2.3	+0.5	81.5
92	Jamaal Williams, GB	RB	96.0	+18.7	2.8	+0.8	87.0
93	Tarik Cohen, Chi	RB	96.7	+2.4	3.1	+0.1	85.9
94	Kirk Cousins, Min	QB	97.1	-7.5	4.8	+0.7	95.0
95	Isaiah Crowell, NYJ	RB	97.9	-14.7	4.3	-0.8	88.0
96	Kerryon Johnson, Det	RB	98.1	+2.8	3.7	+0.1	86.4
97	Randall Cobb, GB	WR	99.8	-17.5	4.2	-0.4	91.0
98	Jordy Nelson, Oak	WR	100.7	-1	3.3	+0.1	89.2
99	Adrian Peterson, Wsh	RB	100.7	+13.2	4.3	+0.7	79.2
100	Kelvin Benjamin, Buf	WR	101.3	+4	2.4	-0.4	86.5
101	Nelson Agholor, Phi	WR	104.0	+17.8	2.1	+0.5	80.7
102	Rashaad Penny, Sea	RB	104.9	-1.9	3.5	-3.7	85.6
103	Sony Michel, NE	RB	107.8	+6.5	2.6	-1.1	80.5
104	Rams D/ST	D/ST	108.3	-15.4	3.8	+0	99.3
105	Jack Doyle, Ind	TE	108.5	-4.1	2.1	+0.1	89.7
106	Josh Doctson, Wsh	WR	108.8	+9.7	1.5	+0.1	72.1
107	Eagles D/ST	D/ST	109.5	-16.8	3.1	+0	98.9
108	James White, NE	RB	109.6	+6.2	1.7	+0.2	71.1
109	Matthew Stafford, Det	QB	110.8	-8.4	3.6	+0.7	90.4
110	Trey Burton, Chi	TE	111.9	-3	3.3	+0.8	86.2
111	Kenny Stills, Mia	WR	112.3	+6.8	1.7	+0.2	76.8
112	Matt Ryan, Atl	QB	114.0	-9.5	3.7	+0.5	85.5
113	Allen Hurns, Dal	WR	115.2	+8.5	1.7	+0.2	73.0
114	Vikings D/ST	D/ST	116.4	-13.5	3.2	+0.3	98.9
115	David Njoku, Cle	TE	117.2	-6.1	2.7	+0.2	83.9
116	Greg Zuerlein, LAR	K	117.5	-12.8	3.0	-0.1	99.5
117	Keelan Cole, Jax	WR	117.9	+52.1	1.6	+1.6	52.8
118	Stephen Gostkowski, NE	K	118.4	-14	3.1	+0.3	99.5
119	Jimmy Garoppolo, SF	QB	118.4	-12.2	4.1	+0.6	83.5
120	Mohamed Sanu, Atl	WR	119.9	+3	1.5	+0.2	71.7
121	Paul Richardson, Wsh	WR	121.8	+12.9	1.1	+0	53.0
122	Jared Goff, LAR	QB	122.4	-1.2	3.4	+0.2	64.0
123	DeVante Parker, Mia	WR	122.6	-0.9	1.4	-0.6	68.6
124	Dak Prescott, Dal	QB	123.0	-6.7	3.4	+0.1	51.5
125	Tyler Lockett, Sea	WR	123.3	+14.5	1.3	+0.1	53.6
126	Patrick Mahomes, KC	QB	123.5	-7.9	3.1	+0.6	79.6
127	Kenny Golladay, Det	WR	123.5	+9.2	1.4	+0.1	61.2
128	Derek Carr, Oak	QB	123.8	-2	3.6	+0.3	33.2
129	Matt Breida, SF	RB	124.2	+45.8	2.1	+2.1	52.2
130	Philip Rivers, LAC	QB	124.7	-6.8	3.2	+0.7	80.2
131	Danny Amendola, Mia	WR	124.8	+7.7	1.3	+0.1	53.4
132	Marlon Mack, Ind	RB	125.4	+4.1	1.9	-0.5	71.8
133	John Ross, Cin	WR	125.5	+9.5	1.5	+0.3	52.0
134	Michael Gallup, Dal	WR	126.2	+8.6	1.6	+0.1	49.1
135	Alex Smith, Wsh	QB	127.1	-9	2.4	+0.1	72.7
136	Chris Godwin, TB	WR	128.3	+15.6	1.2	+0.1	33.4
137	Marcus Mariota, Ten	QB	128.9	-1.9	2.9	+0	49.8
138	Texans D/ST	D/ST	129.5	-12.1	1.9	+0.1	92.1
139	Cameron Meredith, NO	WR	129.9	+3.6	1.3	+0	50.8
140	Tyler Eifert, Cin	TE	130.4	-4.1	1.7	+0.2	62.8
141	Rishard Matthews, Ten	WR	130.7	+11.3	1.1	+0	46.3
142	Justin Tucker, Bal	K	131.2	-10.9	2.3	+0.1	98.9
143	Ravens D/ST	D/ST	131.3	-11.9	1.8	+0.2	96.7
144	Charles Clay, Buf	TE	131.7	+3.8	1.2	+0.1	56.4
145	Giovani Bernard, Cin	RB	132.2	+3.3	1.3	+0	56.4
146	Bilal Powell, NYJ	RB	132.7	+7.1	1.4	+0	48.3
147	Aaron Jones, GB	RB	132.8	+2.3	2.1	+0.3	41.8
148	Theo Riddick, Det	RB	133.2	+4.3	1.2	+0.1	50.6
149	Calvin Ridley, Atl	WR	134.1	-2.1	2.3	+0.1	53.2
150	Antonio Gates, FA	TE	134.2	+35.8	0.0	+0	11.0
151	Ty Montgomery, GB	RB	134.5	-3.4	1.5	-0.1	67.7
152	DJ Moore, Car	WR	134.7	+0.6	1.7	+0.1	44.7
153	LeGarrette Blount, Det	RB	135.2	-1.5	1.4	+0	29.6
154	Jared Cook, Oak	TE	136.3	-4.2	1.5	+0	26.1
155	George Kittle, SF	TE	136.4	+0.1	1.5	+0.1	40.2
156	Mike Wallace, Phi	WR	136.4	+33.6	0.0	+0	20.1
157	Ronald Jones, TB	RB	136.5	-28.3	2.4	-5.7	72.5
158	Benjamin Watson, NO	TE	136.9	-1.3	1.4	+0.2	36.2
159	Bears D/ST	D/ST	137.0	+33	1.7	+1.7	29.3
160	Latavius Murray, Min	RB	137.1	-3.2	1.8	+0.1	48.3
161	Mike Williams, LAC	WR	137.4	-2.5	2.1	-0.2	39.1
162	Patriots D/ST	D/ST	137.5	-9.4	1.4	+0.1	89.4
163	Nick Chubb, Cle	RB	137.7	-4.6	2.4	+0.4	36.8
164	Austin Seferian-Jenkins, Jax	TE	138.1	+0.3	1.5	+0.2	26.0
165	DeSean Jackson, TB	WR	138.2	-0.7	1.5	+0.1	48.8
166	Chris Boswell, Pit	K	139.0	-11.8	1.7	+0	95.3
167	C.J. Anderson, Car	RB	139.3	-1.2	1.6	+0.1	35.0
168	Dez Bryant, FA	WR	140.1	-3.9	2.0	+0.2	39.5
169	Cardinals D/ST	D/ST	140.3	-0.6	1.3	+0.2	27.7
170	Matt Bryant, Atl	K	140.4	-10	1.5	+0.1	94.0
171	James Washington, Pit	WR	140.5	+4.8	1.2	+0	21.3
172	Jordan Wilkins, Ind	RB	140.9	+1.1	1.5	+0.2	31.1
173	Chargers D/ST	D/ST	141.0	-8.9	1.5	+0.1	87.2
174	John Brown, Bal	WR	141.4	+3.2	1.3	-0.1	24.7
175	Courtland Sutton, Den	WR	142.1	-1.9	2.2	+2.2	21.5
176	Doug Martin, Oak	RB	142.3	-2.1	1.3	-0.1	27.1
177	Anthony Miller, Chi	WR	142.7	-0.5	1.7	-0.1	30.0
178	Broncos D/ST	D/ST	143.6	-8.1	1.4	+0	80.6
179	Panthers D/ST	D/ST	144.7	-1.9	1.5	+0.2	48.6
180	Mason Crosby, GB	K	147.1	-0.6	1.4	-0.1	28.4
181	Titans D/ST	D/ST	147.4	-9.1	1.2	+0	56.2
182	Wil Lutz, NO	K	147.9	-4	1.3	+0	92.4
183	Saints D/ST	D/ST	148.6	-6.8	1.1	+0	72.3
184	Robbie Gould, SF	K	150.5	-5.7	1.2	+0	84.4
185	Adam Vinatieri, Ind	K	150.5	-6	1.5	+0	46.2
186	Jake Elliott, Phi	K	151.7	-5.2	1.2	+0	85.0
187	Graham Gano, Car	K	152.0	-3	1.3	+0	36.3
188	Matt Prater, Det	K	152.4	-5.9	1.3	+0.1	81.0
189	Harrison Butker, KC	K	155.5	-4.1	1.2	+0.1	66.9
190	Jerick McKinnon*, SF	RB	170.0	-138.2	25.0	-2	50.4
191	Devontae Booker, Den	RB	170.0	-27.7	1.3	+0.1	32.0
192	Cameron Brate, TB	TE	170.0	-34	1.4	-0.1	29.0
193	Tyrod Taylor, Cle	QB	170.0	+0	2.6	+0.5	28.7
194	Corey Clement, Phi	RB	170.0	+0	1.7	+1.7	28.2
195	Mitchell Trubisky, Chi	QB	170.0	-40.5	2.6	+0	23.3
196	Lions D/ST	D/ST	170.0	+0	0.0	+0	23.0
197	Steelers D/ST	D/ST	170.0	-35.1	1.3	-0.1	21.4
198	Donte Moncrief, Jax	WR	170.0	+0	0.0	+0	21.1
199	Austin Hooper, Atl	TE	170.0	-37.5	1.4	-0.1	20.2
200	Jeremy Hill, NE	RB	170.0	-28	0.0	+0