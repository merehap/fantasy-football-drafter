use std::cmp::Ordering;
use std::collections::{HashSet, HashMap, VecDeque};
use std::time::SystemTime;
use std::thread;

use rand::prelude::*;

use draft::Draft;
use drafter;
use keyboard;
use player::{Player, Position};
use players::Players;
use roster::Roster;

pub trait Drafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        substitutes: &HashMap<Position, Player>,
        draft: Option<Draft>) -> Player;
    fn desc(&self) -> String;
}

pub struct OpponentDrafter {
    rng: ThreadRng,
}

impl OpponentDrafter {
    pub fn new() -> OpponentDrafter {
        OpponentDrafter{rng: thread_rng()}
    }
}

impl Drafter for OpponentDrafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        _: &HashMap<Position, Player>,
        _: Option<Draft>) -> Player {

        let mut candidates = vec![];
        for position in next_positions(roster) {
            candidates.extend_from_slice(&players.players_by_adp_for_position(position).clone()[..5]);
        }

        let cand_total: f32 = candidates.iter()
            .map(|c| c.adp)
            .sum();

        let cand_minimum = candidates.iter().fold(1.0/0.0, |min,c| f32::min(min, cand_total - c.adp));
        let vor_minimum = candidates.iter().fold(1.0/0.0, |min,c| f32::min(min, c.projected_vor));

        let weights: Vec<f32> = candidates.iter()
            .map(|c| (cand_total - c.adp - cand_minimum + 1.0).powi(2) +
                (c.projected_vor - vor_minimum).powi(4))
            .collect();

        let total_weight: f32 = weights.iter().sum();

        let probabilities: Vec<f32> = weights.iter()
            .map(|w| w / total_weight)
            .collect();

        let mut player_value: f32 = self.rng.gen();
        for i in 0..probabilities.len() {
            if player_value <= probabilities[i] {
                return candidates[i].clone();
            }

            player_value -= probabilities[i];
        }

        panic!("Probabilities didn't add up to 1? Stupid floating point numbers...");
    }

    fn desc(&self) -> String {
        "OpponentDrafter".to_string()
    }
}

pub struct MonteCarloDrafter {

}

impl MonteCarloDrafter {
    pub fn new() -> MonteCarloDrafter {
        MonteCarloDrafter{}
    }
}

impl Drafter for MonteCarloDrafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        substitutes: &HashMap<Position, Player>,
        draft: Option<Draft>) -> Player {

        let mut candidates = vec![];
        for position in next_positions(roster) {
            candidates.extend_from_slice(&players.players_by_vor_for_position(position).clone()[..3]);
        }

        println!(
            "Starting monte carlo simulation. This may take a while. Considering the following players: {:?}",
            candidates.iter().map(|c| c.clone().name).collect::<Vec<_>>());
        let start_time = SystemTime::now();

        let mut candidate_wins = vec![];
        let mut threads = vec![];
        for candidate in candidates {
            let draft = draft.clone().expect("No drafter provided!");
            let temp_substitutes = substitutes.clone();
            threads.push(thread::spawn(move || {
                let mut total_wins = 0.0;
                let iteration_count = 150;

                for _ in 0..iteration_count {
                    let mut sim_draft = draft.clone();
                    if !sim_draft.is_my_turn() {
                        panic!("MonteCarloDrafter called but it isn't my turn!");
                    }

                    // Our current turn
                    sim_draft.force_pick(&candidate);
                    while !sim_draft.is_over() {
                        let mut drafter: Box<Drafter> = if sim_draft.is_my_turn() {
                            // Substitute VorDrafter for MonteCarlo for later rounds to
                            // greatly speed up simulation.
                            Box::new(NaiveVorDrafter::new())
                        } else {
                            Box::new(OpponentDrafter::new())
                        };

                        sim_draft.advance_fixed_drafter(&mut *drafter, &temp_substitutes);
                    }

                    let mut matchups = sim_draft.season_matchups_mut();
                    let temp = matchups.expected_win_stats(
                        &draft.teams(), &draft.my_name(), &temp_substitutes, 250).0;
                    total_wins += temp;
                }

                let expected_win_count = total_wins / (iteration_count as f32);
                (candidate.clone(), expected_win_count)
            }));
        }

        for thread in threads {
            // Wait for the thread to finish.
            candidate_wins.push(thread.join().expect("Thread didn't finish?"));
        }


        let end_time = SystemTime::now();
        println!("MonteCarloDrafter took {:?}", end_time.duration_since(start_time).expect("Negative duration?"));

        let cmp_floats = |f0: &f32, f1: &f32| {
            if f0 >= f1 {
                Ordering::Less
            } else {
                Ordering::Greater
            }
        };

        candidate_wins.sort_by(|(_, a),(_,b)| cmp_floats(a, b));
        for (candidate, wins) in &candidate_wins {
            println!(
                "\t\tSimulating picking {:?} results in {} wins out of 14 games.",
                candidate.name,
                wins);
        }

        candidate_wins[0].clone().0
    }

    fn desc(&self) -> String {
        "MonteCarloDrafter".to_string()
    }
}

pub struct KeyboardDrafter {
    comparison_drafter: Box<Drafter>,
}

impl KeyboardDrafter {
    pub fn new(comparison_drafter: Box<Drafter>) -> KeyboardDrafter {
        KeyboardDrafter{comparison_drafter}
    }
}

impl Drafter for KeyboardDrafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        substitutes: &HashMap<Position, Player>,
        draft: Option<Draft>) -> Player {

        let suggestion = if draft.clone().unwrap().is_my_turn() {
            println!("It's your turn!");
            let reference_player = self.comparison_drafter.pick_player(players, roster, substitutes, draft);
            println!("You should draft: {}", reference_player.name);
            Some(reference_player)
        } else {
            None
        };

        loop {
            if let Some(pl) = keyboard::get_player_from_keyboard(&players) {
                return pl;
            } else if let Some(sug) = suggestion {
                println!("Drafting suggested player, {}", sug.name);
                return sug;
            }

            println!("You didn't select a player to draft!");
        }
    }

    fn desc(&self) -> String {
        format!("Keyboard Drafter, compared with {{{}}}", self.comparison_drafter.desc()).to_string()
    }
}

pub struct PredeterminedDrafter {
    predetermined_players: VecDeque<Player>,
    comparison_drafter: Box<Drafter>,
    adp_drafter: Box<Drafter>,
}

impl PredeterminedDrafter {
    pub fn new(
        predetermined_players: VecDeque<Player>,
        comparison_drafter: Box<Drafter>) -> PredeterminedDrafter {

        PredeterminedDrafter{
            predetermined_players,
            comparison_drafter,
            adp_drafter: Box::new(drafter::NaiveAdpDrafter::new())}
    }
}

impl Drafter for PredeterminedDrafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        substitutes: &HashMap<Position, Player>,
        _: Option<Draft>) -> Player {

        let player = self.predetermined_players.pop_front()
            .expect("No more predefined players left to draft!");
        let reference_player = self.comparison_drafter.pick_player(players, roster, substitutes, None);
        let top_player_by_adp = self.adp_drafter.pick_player(players, roster, substitutes, None);

        println!(
            "Predetermined pick {} had a difference of {} VOR from reference pick {}. ADP Diff: {}, Pos {:?} vs {:?}",
            player.name,
            player.projected_vor - reference_player.projected_vor,
            reference_player.name,
            top_player_by_adp.adp - player.adp,
            player.position,
            top_player_by_adp.position);

        player
    }

    fn desc(&self) -> String {
        format!("Predetermined Order, compared with {{{}}}", self.comparison_drafter.desc())
    }
}

pub struct NaiveVorDrafter {

}

impl NaiveVorDrafter {
    pub fn new() -> NaiveVorDrafter {
        NaiveVorDrafter{}
    }
}

impl Drafter for NaiveVorDrafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        _: &HashMap<Position, Player>,
        _: Option<Draft>) -> Player {

        pick_best(
            players,
            &next_positions(roster),
            |pls, po| pls.players_by_vor_for_position(po)[0].clone(),
            |p0, p1| p0.projected_vor > p1.projected_vor)
    }

    fn desc(&self) -> String {
        "VOR (Naive)".to_string()
    }
}

#[derive(Clone)]
pub struct NaiveAdpDrafter {

}

impl NaiveAdpDrafter {
    pub fn new() -> NaiveAdpDrafter {
        NaiveAdpDrafter{}
    }
}

impl Drafter for NaiveAdpDrafter {
    fn pick_player(
        &mut self,
        players: &mut Players,
        roster: &Roster,
        _: &HashMap<Position, Player>,
        _: Option<Draft>) -> Player {

        pick_best(
            players,
            &next_positions(roster),
            |pls, po| pls.players_by_adp_for_position(po)[0].clone(),
            |p0, p1| p0.adp < p1.adp)
    }

    fn desc(&self) -> String {
        "ADP (Naive)".to_string()
    }
}

fn pick_best<G, C>(
    players: &mut Players,
    positions: &HashSet<Position>,
    get_player: G,
    is_better: C) -> Player
    where G: Fn(&Players, Position) -> Player,
          C: Fn(&Player, &Player) -> bool {

    let mut pos_iter = positions.iter();
    if let Some(&first_pos) = pos_iter.next() {
        let mut best_player = get_player(players, first_pos);
        for &position in pos_iter {
            let player = get_player(players, position);
            if is_better(&player, &best_player) {
                best_player = player;
            }
        }

        best_player
    } else {
        panic!("No players at any position left?");
    }
}

fn next_positions(roster: &Roster) -> HashSet<Position> {
    let mut positions = HashSet::new();
    use player::Position::*;
    let qb_count = roster.position_count(Quarterback);
    let rb_count = roster.position_count(RunningBack);
    let wr_count = roster.position_count(WideReceiver);
    let te_count = roster.position_count(TightEnd);
    let k_count = roster.position_count(Kicker);
    let d_count = roster.position_count(Defense);

    if qb_count == 0 {
        positions.insert(Quarterback);
    }

    if rb_count < 2 {
        positions.insert(RunningBack);
    }

    if wr_count < 2 {
        positions.insert(WideReceiver);
    }

    if te_count == 0 {
        positions.insert(TightEnd);
    }

    if qb_count >= 1 && rb_count >= 2 && wr_count >= 2 && te_count >= 1 {
        positions.insert(Quarterback);
        positions.insert(RunningBack);
        positions.insert(WideReceiver);
        positions.insert(TightEnd);
        if rb_count >= 3 && wr_count >= 3 {
            positions.insert(Kicker);
            positions.insert(Defense);
        }
    }

    if qb_count > 0 {
        positions.remove(&Quarterback);
    }

    if rb_count > 4 {
        positions.remove(&RunningBack);
    }

    if wr_count > 4 {
        positions.remove(&WideReceiver);
    }

    if te_count > 1 {
        positions.remove(&TightEnd);
    }

    if k_count > 0 {
        positions.remove(&Kicker);
    }

    if d_count > 0 {
        positions.remove(&Defense);
    }

    if positions.is_empty() {
        positions.insert(RunningBack);
        positions.insert(WideReceiver);
        positions.insert(TightEnd);
    }

    positions
}
