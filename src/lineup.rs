use rand::prelude::*;

use player::{Player, Position};
use week::Week;

const WEEK_COUNT: f32 = 13.0;

#[derive(Debug)]
pub struct Lineup {
    quarterback: Option<Player>,
    running_backs: Vec<Player>,
    wide_receivers: Vec<Player>,
    tight_end: Option<Player>,
    kicker: Option<Player>,
    defense: Option<Player>,
    flex: Option<Player>,
    week: Week,
    rng: ThreadRng,
}

impl Lineup {
    pub fn projected_week_point_total(&self) -> f32 {
        self.players().iter()
            .filter(|p| p.bye_week != self.week)
            .map(|p| p.projected_points_p50 / WEEK_COUNT)
            .sum()
    }

    pub fn gen_possible_week_point_total(&mut self) -> f32 {
        let mut total = 0.0;
        for player in self.players() {
            if player.bye_week == self.week {
                continue;
            }

            let p50 = player.projected_points_p50 / WEEK_COUNT;
            let p10 = p50 + (player.projected_points_p10 - player.projected_points_p50) / WEEK_COUNT.sqrt();
            let p90 = p50 + (player.projected_points_p90 - player.projected_points_p50) / WEEK_COUNT.sqrt();

            let x: f32 = self.rng.gen();
            let sample =
                p50 +
                    0.5 * f32::ln((1.0 - 0.1) / 0.1).powi(-1) * (p90 - p10) * f32::ln(x / (1.0 - x)) +
                    ((1.0 - 2.0 * 0.1) * (f32::ln((1.0 - 0.1) / 0.1))).powi(-1) * (1.0 - 2.0 * (p50 - p10) / (p90 - p10)) * (p90 - p10) * (x - 0.5) * f32::ln(x / (1.0 - x));
            total += sample;
        }

        total
    }

    pub fn players(&self) -> Vec<Player> {
        let mut result = vec![];

        Lineup::push_if_some(&mut result, self.quarterback.clone());
        result.append(&mut self.running_backs.clone());
        result.append(&mut self.wide_receivers.clone());
        Lineup::push_if_some(&mut result, self.tight_end.clone());
        Lineup::push_if_some(&mut result, self.flex.clone());
        Lineup::push_if_some(&mut result, self.kicker.clone());
        Lineup::push_if_some(&mut result, self.defense.clone());

        result
    }

    fn push_if_some<T>(vec: &mut Vec<T>, value: Option<T>) {
        if let Some(v) = value {
            vec.push(v);
        }
    }
}

pub struct LineupBuilder {
    pub quarterback: Option<Player>,
    pub running_backs: Vec<Player>,
    pub wide_receivers: Vec<Player>,
    pub tight_end: Option<Player>,
    pub kicker: Option<Player>,
    pub defense: Option<Player>,
    flex: Option<Player>,
    week: Week,
}

impl LineupBuilder {
    pub fn new(week: Week) -> LineupBuilder {
        LineupBuilder {
            quarterback: None,
            running_backs: vec![],
            wide_receivers: vec![],
            tight_end: None,
            kicker: None,
            defense: None,
            flex: None,
            week,
        }
    }

    pub fn add_positioned_player(&mut self, player: Player) {
        match player.position {
            Position::Quarterback => LineupBuilder::set_or_panic(&mut self.quarterback, player),
            Position::TightEnd    => LineupBuilder::set_or_panic(&mut self.tight_end, player),
            Position::Kicker      => LineupBuilder::set_or_panic(&mut self.kicker, player),
            Position::Defense     => LineupBuilder::set_or_panic(&mut self.defense, player),
            Position::RunningBack  => LineupBuilder::append_or_panic(&mut self.running_backs, player),
            Position::WideReceiver => LineupBuilder::append_or_panic(&mut self.wide_receivers, player),
        }
    }

    pub fn add_flex_player(&mut self, player: Player) {
        if self.flex.is_some() {
            panic!("Flex is already set.");
        }

        self.flex = Some(player);
    }

    pub fn build(&self) -> Lineup {
        assert!(self.wide_receivers.len() <= 2, "Too many wide receivers.");
        assert!(self.running_backs.len() <= 2, "Too many running backs.");

        Lineup {
            quarterback: self.quarterback.clone(),
            tight_end:   self.tight_end.clone(),
            kicker:      self.kicker.clone(),
            defense:     self.defense.clone(),
            flex:        self.flex.clone(),
            wide_receivers: self.wide_receivers.clone(),
            running_backs:  self.running_backs.clone(),
            week: self.week,
            rng: thread_rng(),
        }
    }

    fn set_or_panic(target: &mut Option<Player>, new: Player) {
        if target.is_some() {
            panic!("{:?} already set.", new.position);
        }

        target.get_or_insert(new);
    }

    fn append_or_panic(target: &mut Vec<Player>, player: Player) {
        if target.len() == 2 {
            panic!("Can't add another {:?}.", player.position);
        }

        target.push(player);
    }
}