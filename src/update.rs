use player::PlayerName;
use team::TeamName;

pub enum Update {
    Add(TeamName, PlayerName),
    Drop(TeamName, PlayerName),
    Waiver(TeamName, PlayerName, PlayerName),
    Trade(TeamName, Vec<PlayerName>, TeamName, Vec<PlayerName>),
}