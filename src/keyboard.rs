use std::borrow::Borrow;
use std::hash::Hash;
use std::io;

use indexmap::map::IndexMap;

use player::{Player, PlayerName};
use players::Players;

pub fn get_player_from_keyboard(players: &Players) -> Option<Player> {
    let mut default_player: Option<Player> = None;
    let player_names = players.players_by_vor();
    let player_names: Vec<String> = player_names.iter()
        .map(|p| p.clone().name.to_raw().to_string())
        .collect();
    loop {
        println!("Please input the name of your draft choice:");
        let mut player_name = String::new();
        io::stdin().read_line(&mut player_name)
            .expect("Read line failed?");
        player_name = player_name.trim().to_string();
        if player_name.is_empty() {
            if let Some(ref def) = default_player {
                println!("You have selected the first player in the list, {}.", def.name);
                return Some(def.clone());
            } else {
                return None;
            }
        }

        if let Some(pl) = players.by_name(&PlayerName::new(&player_name)) {
            return Some(pl);
        } else {
            let prefix_matches = match_prefix(&player_name, &player_names);
            println!("{} possible players found: ", prefix_matches.len());
            for prefix_match in &prefix_matches {
                println!("{}", prefix_match);
            }

            if prefix_matches.is_empty() {
                continue;
            }

            default_player = players.by_name(&PlayerName::new(&prefix_matches.get(0).cloned().unwrap()));
        }
    }
}

pub fn lookup_type_from_keyboard<K: Into<String> + Eq + Hash + Clone + Borrow<String>, V: Clone>(
    lookup: &IndexMap<K, V>) -> Option<V> {

    let mut default: Option<V> = None;
    loop {
        println!("Please input your choice:");
        let mut player_name = String::new();
        io::stdin().read_line(&mut player_name)
            .expect("Read line failed?");
        player_name = player_name.trim().to_string();
        if player_name.is_empty() {
            if let Some(def) = default {
                println!("You have selected the first choice in the list.");
                return Some(def);
            } else {
                return None;
            }
        }

        if let Some(ref pl) = lookup.get(&player_name) {
            return Some((**pl).clone());
        } else {
            let keys: Vec<String> = lookup.keys()
                .map(|k| {let r: String = k.clone().into(); r})
                .collect();
            let prefix_matches: Vec<String> = match_prefix(&player_name, &keys);
            if prefix_matches.is_empty() {
                println!("No matches!");
                continue;
            }

            println!("{} possible matches found: ", prefix_matches.len());
            for prefix_match in &prefix_matches {
                println!("\t{}", prefix_match);
            }

            if prefix_matches.is_empty() {
                continue;
            }

            default = lookup.get(&prefix_matches.get(0).cloned().unwrap()).cloned();
        }
    }
}

pub fn match_prefix(prefix: &str, candidates: &[String]) -> Vec<String> {
    let mut matches = vec![];
    for candidate in candidates {
        if candidate.starts_with(prefix)
            || candidate.to_lowercase().starts_with(&prefix.to_lowercase()) {

            matches.push(candidate.clone());
        }
    }

    matches
}