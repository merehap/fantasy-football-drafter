extern crate csv;
extern crate indexmap;
extern crate num_rational;
extern crate rand;

mod draft;
mod drafter;
mod draft_result_loader;
mod keyboard;
mod lineup;
mod lineup_selector;
mod matchup;
mod player;
mod players;
mod player_loader;
mod roster;
mod season;
mod season_loader;
mod team;
mod trade;
mod update;
mod update_loader;
mod week;

use std::io;
use std::fmt;
use std::path::PathBuf;

fn main() {
    println!("Which season should be used?");
    let mut season_name = String::new();
    io::stdin().read_line(&mut season_name)
        .expect("Failed to read season name from console.");
    let season_name = season_name.trim();

    let season_path = data_file_path(season_name, "season");
    let rankings_path = data_file_path(season_name, "rankings");
    let adp_path = data_file_path(season_name, "adp");
    let draft_path = data_file_path(season_name, "draft");
    let updates_path = data_file_path(season_name, "updates").expect("Updates path needed.");

    println!("Which draft mode should be executed?");
    let mut raw_mode = String::new();
    io::stdin().read_line(&mut raw_mode)
        .expect("Failed to read draft mode from console.");


    let players = player_loader::load_players_from_csv(
        rankings_path.expect("No ranking data specified!"),
        adp_path);
    let (season, mut drafters) = season_loader::load_season_from_rounds(
        season_path.expect("No season data specified!"));
    let updates = update_loader::load_updates(updates_path);

    let raw_mode = raw_mode.trim();
    match Mode::from_prefix(&raw_mode) {
        Some(Mode::Analysis) => {
            println!("Starting draft analysis.");
            draft::draft_analysis(
                season, &players, updates, draft_path.expect("No draft path specified!"));
        },
        Some(Mode::Static) => {
            println!("Starting static draft.");
            draft::static_draft(season, &players, updates, &mut drafters);
        },
        Some(Mode::Keyboard) => {
            println!("Starting keyboard draft.");
            draft::keyboard_draft(season, &players, updates, &mut drafters);
        },
        Some(Mode::Decision) => {
            println!("Starting decision mode.");
            draft::decision_mode(
                season, &players, updates, draft_path.expect("No draft path specified!"));
        },
        None => panic!("Unrecognized draft mode '{}'", raw_mode),
    }
}

#[derive(Clone, Debug)]
enum Mode {
    Analysis,
    Static,
    Keyboard,
    Decision,
}

impl fmt::Display for Mode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl Mode {
    fn from_prefix(prefix: &str) -> Option<Mode> {
        if prefix.is_empty() {
            return None;
        }

        use Mode::*;
        for mode in &[Analysis, Static, Keyboard, Decision] {
            if mode.to_string().to_lowercase().starts_with(&prefix.to_lowercase()) {
                return Some(mode.clone());
            }
        }

        None
    }
}

fn data_file_path<'a>(season_name: &'a str, file_name_prefix: &'a str) -> Option<PathBuf> {
    let mut path_buf = PathBuf::from("./seasons");
    path_buf.push(season_name);

    for file_name in path_buf.read_dir()
        .expect("Failed to read season directory") {

        let file_name = file_name
            .expect("Bad something")
            .file_name();
        if file_name
            .clone()
            .into_string()
            .expect("Non UTF8 data?")
            .starts_with(file_name_prefix) {

            path_buf.push(file_name);
            return Some(path_buf);
        }
    }

    None
}
