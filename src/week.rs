#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Week {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Eleven,
    Twelve,
    Thirteen,
    Fourteen,
    Fifteen,
    Sixteen,
}

impl Week {
    pub fn new(week: u8) -> Week {
        match week {
            1 => Week::One,
            2 => Week::Two,
            3 => Week::Three,
            4 => Week::Four,
            5 => Week::Five,
            6 => Week::Six,
            7 => Week::Seven,
            8 => Week::Eight,
            9 => Week::Nine,
            10 => Week::Ten,
            11 => Week::Eleven,
            12 => Week::Twelve,
            13 => Week::Thirteen,
            14 => Week::Fourteen,
            15 => Week::Fifteen,
            16 => Week::Sixteen,
            _  => panic!("Invalid Week {}", week)
        }
    }
}

