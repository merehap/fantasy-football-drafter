use std::collections::HashMap;

use indexmap::map::IndexMap;

use matchup::Matchup;
use player::{Player, Position};
use team::{Team, TeamName};
use week::Week;

pub struct Season {
    pub teams: IndexMap<TeamName, Team>,
    pub my_name: TeamName,
    pub season_matchups: SeasonMatchups,
}

impl Season {
    pub fn new(
        teams: IndexMap<TeamName, Team>,
        my_name: TeamName,
        season_matchups: SeasonMatchups) -> Season {

        Season {teams, my_name, season_matchups}
    }
}

#[derive(Clone)]
pub struct SeasonMatchups {
    season_matchups: HashMap<TeamName, Vec<Matchup>>,
}

impl SeasonMatchups {
    pub fn from_week_matchups(week_matchups: &[WeekMatchups]) -> SeasonMatchups {
        let mut season_matchups = HashMap::new();
        for matchup in &week_matchups[0].week_matchups {
            let matchup = matchup.clone();
            season_matchups.insert(matchup.left_team_name, vec![]);
            season_matchups.insert(matchup.right_team_name, vec![]);
        }

        for raw_week in 1..=week_matchups.len() {
            for matchup in &week_matchups[raw_week-1].week_matchups {
                let matchup = matchup.clone();
                season_matchups
                    .get_mut(&matchup.left_team_name)
                    .expect("Missing a position?")
                    .push(matchup.clone());
                season_matchups
                    .get_mut(&matchup.right_team_name)
                    .expect("Missing a position?")
                    .push(matchup.clone());
            }
        }

        SeasonMatchups{season_matchups}
    }

    pub fn expected_win_stats(
        &mut self,
        teams: &IndexMap<TeamName, Team>,
        team_name: &TeamName,
        substitutes: &HashMap<Position, Player>,
        sample_count: u32) -> (f32, Vec<f32>) {

        let player_matchups = &mut self.season_matchups.get_mut(team_name).unwrap();
        let mut results = vec![];
        for mut matchup in player_matchups.iter_mut() {
            let prob = matchup.team_win_rate_probability(team_name, teams, substitutes, sample_count);
            results.push(prob);
        }

        (results.iter().sum(), results)
    }
}

#[derive(Clone)]
pub struct WeekMatchups {
    pub week: Week,
    pub week_matchups: Vec<Matchup>,
}

impl WeekMatchups {
    pub fn new(week: Week, week_matchups: Vec<Matchup>) -> WeekMatchups {
        WeekMatchups{week, week_matchups}
    }
}