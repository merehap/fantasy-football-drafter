use std::collections::HashMap;
use std::path::{PathBuf};

use indexmap::map::IndexMap;

use drafter::*;
use draft_result_loader;
use keyboard;
use lineup_selector;
use player::{Player, PlayerName, Position};
use players::Players;
use season::{Season, SeasonMatchups};
use team::{Team, TeamName};
use trade::Trade;
use update::Update;
use week::Week;


pub fn static_draft(
    season: Season,
    players: &Players,
    updates: Vec<Update>,
    mut drafters: &mut Vec<Box<Drafter>>) {

    run_draft(season, &players, updates, &mut drafters, 14);
}

pub fn keyboard_draft(
    season: Season,
    players: &Players,
    updates: Vec<Update>,
    mut drafters: &mut Vec<Box<Drafter>>) {

    run_draft(season, &players, updates, &mut drafters, 14);
}

pub fn draft_analysis(
    season: Season,
    players: &Players,
    updates: Vec<Update>,
    draft_results_path: PathBuf) {

    let mut drafters: Vec<Box<Drafter>> =
        draft_result_loader::load_results_from_rounds(draft_results_path, &players);

    run_draft(season, &players, updates, &mut drafters, 14);
}

pub fn decision_mode(
    season: Season,
    players: &Players,
    updates: Vec<Update>,
    draft_results_path: PathBuf) {

    let mut drafters = draft_result_loader::load_results_from_rounds(
        draft_results_path,
        &players);

    let substitutes = substitutes(&players);
    let mut draft = run_draft(season, &players, updates, &mut drafters, 15);

    loop {
        println!("What would you like to do?");
        let mut actions = IndexMap::new();
        actions.insert("checktrade".to_string(), Action::CheckTrade);
        actions.insert("besttrades".to_string(), Action::BestTrades);
        actions.insert("update".to_string(), Action::Update);
        match keyboard::lookup_type_from_keyboard(&actions) {
            Some(Action::CheckTrade) => {
                let draft_before = draft.clone();

                println!("First team name?");
                let mut first_team = keyboard::lookup_type_from_keyboard(&draft.teams()).unwrap();
                println!("Second team name?");
                let mut second_team = keyboard::lookup_type_from_keyboard(&draft.teams()).unwrap();

                let mut season_matchups = draft.clone().season_matchups_mut().clone();
                let win_count = season_matchups.expected_win_stats(
                    &draft.teams(), &first_team.name(), &substitutes, 10_000).0;
                println!("Winning odds before: {}", win_count);

                println!("First team offer?");
                let mut first_team_offer = vec![];
                while let Some(player) = keyboard::get_player_from_keyboard(&players) {
                    first_team_offer.push(draft.teams_mut()[&first_team.name()].roster_mut().remove(&player.name).expect("Unknown player."));
                }

                println!("Second team offer?");
                let mut second_team_offer = vec![];
                while let Some(player) = keyboard::get_player_from_keyboard(&players) {
                    second_team_offer.push(draft.teams_mut()[&second_team.name()].roster_mut().remove(&player.name).expect("Unknown player."));
                }

                for player in first_team_offer {
                    draft.teams_mut()[&second_team.name()].roster_mut().add(player);
                }

                for player in second_team_offer {
                    draft.teams_mut()[&first_team.name()].roster_mut().add(player);
                }

                println!("\n{}:", first_team.name().to_raw());
                compare_matchup_outcomes(&draft_before, &draft, &substitutes, &first_team.name());
                println!("\n{}", second_team.name().to_raw());
                compare_matchup_outcomes(&draft_before, &draft, &substitutes, &second_team.name());
            },
            Some(Action::BestTrades) => {
                println!("Team name?");
                let mut my_team = keyboard::lookup_type_from_keyboard(&draft.teams()).unwrap();

                println!("\nLINEUP WEEK 1");
                for player in lineup_selector::simple_select_lineup(
                    my_team.roster(), &substitutes, Week::One).players() {
                    println!("{:?}", player);
                }

                for (opponent_name, opponent) in draft.teams().clone() {
                    let my_team = my_team.clone();
                    if my_team.name() != opponent_name {
                        println!("All trades with {:?}", opponent_name);
                        find_trades(&draft, &my_team, &opponent, &substitutes);
                    }
                }
            },
            Some(Action::Update) => {
                let mut sub_actions = IndexMap::new();
                sub_actions.insert("add".to_string(), UpdateSubAction::Add);
                sub_actions.insert("drop".to_string(), UpdateSubAction::Drop);
                sub_actions.insert("trade".to_string(), UpdateSubAction::Trade);
                println!("Which update sub-action would you like to perform?");
                match keyboard::lookup_type_from_keyboard(&sub_actions) {
                    Some(UpdateSubAction::Add) => {
                        println!("Which team added a player?");
                        let mut team = keyboard::lookup_type_from_keyboard(&draft.teams())
                            .expect("You failed to specify a team.");
                        println!("Player to add?");
                        let player = keyboard::get_player_from_keyboard(&players)
                            .expect("You failed to specify a player.");

                        let player = draft.remove_player(&player);
                        draft.teams_mut()[&team.name()].roster_mut().add(player);
                    },
                    Some(UpdateSubAction::Drop) => {},
                    Some(UpdateSubAction::Trade) => {},
                    Some(UpdateSubAction::Save) => {},
                    None => panic!("Unknown Update SubAction!"),
                }
            },
            None => panic!("Unrecognized action type."),
        }
    }
}

fn compare_matchup_outcomes(
    draft_before: &Draft,
    draft_after: &Draft,
    substitutes: &HashMap<Position, Player>,
    team_name: &TeamName) {

    let mut day_matchups_before = draft_before.clone().season_matchups_mut().clone();
    let mut day_matchups_after = draft_after.clone().season_matchups_mut().clone();

    let (win_count_before, win_probs_before) = day_matchups_before.expected_win_stats(
        &draft_before.teams(), &team_name, &substitutes, 10_000);
    let (win_count_after, win_probs_after) = day_matchups_after.expected_win_stats(
        &draft_after.teams(), &team_name, &substitutes, 10_000);

    println!("Win counts (diff [before/after]): {:.2} [{:.2}/{:.2}]",
        win_count_after - win_count_before, win_count_before, win_count_after);
    for i in 0..win_probs_before.len() {
        println!("Week {}: {:.2} [{:.2}/{:.2}]",
            i+1, win_probs_after[i] - win_probs_before[i], win_probs_before[i], win_probs_after[i]);
    }
}

fn find_trades(
    draft: &Draft,
    first_team: &Team,
    second_team: &Team,
    substitutes: &HashMap<Position, Player>) {

    let mut season_matchups = draft.clone().season_matchups_mut().clone();
    let my_win_count_before = season_matchups.expected_win_stats(
        &draft.teams(), &first_team.name(), &substitutes, 10_000).0;
    let other_win_count_before = season_matchups.expected_win_stats(
        &draft.teams(), &second_team.name(), &substitutes, 10_000).0;
    println!("My win count before, their win count before:");
    println!("{:.2} and {:.2}", my_win_count_before, other_win_count_before);

    let team0_lineup_players = lineup_selector::simple_select_lineup(
        first_team.roster(), &substitutes, Week::Two).players();
    let team1_lineup_players = lineup_selector::simple_select_lineup(
        second_team.roster(), &substitutes, Week::Two).players();

    let mut potential_trades = vec![];
    for i0_first in 0..team0_lineup_players.len() {
        for i0_second in i0_first+1..team0_lineup_players.len() {
            for i1_first in 0..team1_lineup_players.len() {
                for i1_second in i1_first+1..team1_lineup_players.len() {
                    potential_trades.push(Trade::new(
                        vec![team0_lineup_players[i0_first].clone(), team0_lineup_players[i0_second].clone()],
                        vec![team1_lineup_players[i1_first].clone(), team1_lineup_players[i1_second].clone()],
                    ));
                }
            }
        }
    }

    potential_trades = potential_trades.iter()
        .filter(|t| {
            let (p0, p1) = t.total_projected_points();
            (p0 - p1).abs() < 26.0
        })
        .cloned()
        .collect();

    for trade in potential_trades {
        let mut draft = draft.clone();
        for player in trade.team0_offer.clone() {
            draft.teams_mut()[&first_team.name()].roster_mut().remove(&player.name);
            draft.teams_mut()[&second_team.name()].roster_mut().add(player);
        }

        for player in trade.team1_offer.clone() {
            draft.teams_mut()[&second_team.name()].roster_mut().remove(&player.name);
            draft.teams_mut()[&first_team.name()].roster_mut().add(player);
        }

        let mut season_matchups = draft.clone().season_matchups_mut().clone();
        let my_win_count = season_matchups.expected_win_stats(
            &draft.teams(), &first_team.name(), &substitutes, 2_000).0;
        let other_win_count = season_matchups.expected_win_stats(
            &draft.teams(), &second_team.name(), &substitutes, 2_000).0;
        let (p0, p1) = trade.total_projected_points();
        let my_gain = my_win_count - my_win_count_before;
        let other_gain = other_win_count - other_win_count_before;
        if my_gain > 0.2 && other_gain > 0.0 {
            if other_gain < 0.2 {
                println!("Lopsided trade:");
            }
            println!(
                "{} and {} for {} and {}\n\tMy win count after: {:.2}. Other win count after: {:.2}. Points diff: {:.2}.\n",
                trade.team0_offer[0].name,
                trade.team0_offer[1].name,
                trade.team1_offer[0].name,
                trade.team1_offer[1].name,
                my_gain,
                other_gain,
                p0 - p1);
        }
    }
}

#[derive(Clone)]
pub enum Action {
    CheckTrade,
    BestTrades,
    Update,
}

#[derive(Clone)]
pub enum UpdateSubAction {
    Add,
    Drop,
    Trade,
    Save,
}

fn run_draft(
    season: Season,
    players: &Players,
    updates: Vec<Update>,
    drafters: &mut Vec<Box<Drafter>>,
    round_count: usize) -> Draft {

    let mut draft = Draft::new(
        players.clone(), season.teams, season.my_name, season.season_matchups, round_count);

    let substitutes: HashMap<Position, Player> = substitutes(&players);

    while !draft.is_over() {
        println!("{}'s turn (round {}):", draft.team().desc(), draft.round());
        let chosen_player = draft.advance(drafters, &substitutes);
        println!("\tDraft pick: {:?}", chosen_player);
        println!();
    }

    for update in updates {
        process_update(&update, &mut draft);
    }

    println!();
    for (_, team) in draft.teams().iter() {
        println!("{} projected: {}", team.desc(), team.roster().projected_season_point_total(&substitutes));
    }

    println!("\nMY TEAM ROSTER");
    for player in draft.teams()[&draft.my_name].roster().players() {
        println!("{:?}", player);
    }

    let mut day_matchups = draft.clone().season_matchups_mut().clone();

    let (win_count, win_probs) = day_matchups.expected_win_stats(
        &draft.teams(), &draft.my_name, &substitutes, 2_000);
    println!("\nWin ratios");
    for win_ratio in win_probs {
        println!("Win prob: {}", win_ratio);
    }

    println!("Projected total wins: {}", win_count);

    draft
}

fn process_update(update: &Update, draft: &mut Draft) {
    match update {
        Update::Trade(team0_name, team0_player_names, team1_name, team1_player_names) => {
            for player_name in team0_player_names {
                let player = draft
                    .teams_mut()[team0_name]
                    .roster_mut()
                    .remove(player_name)
                    .expect(&format!("Player didn't exist! {}", player_name));
                draft.teams_mut()[team1_name].roster_mut().add(player);
            }

            for player_name in team1_player_names {
                let player = draft
                    .teams_mut()[team1_name]
                    .roster_mut()
                    .remove(player_name)
                    .expect(&format!("Player didn't exist! {}", player_name));
                draft.teams_mut()[team0_name].roster_mut().add(player);
            }
        },
        Update::Add(team_name, player_name) => {
            let player = draft.players.by_name(&player_name).expect("Unknown player.");
            draft.players.remove(&player);
            draft.teams_mut()[team_name].roster_mut().add(player);
        },
        Update::Drop(team_name, player_name) => {
            let player = draft
                .teams_mut()[team_name]
                .roster_mut()
                .remove(player_name)
                .expect("Unknown player.");
            draft.players.add(&player);
        },
        Update::Waiver(team_name, add_player_name, drop_player_name) => {
            let player = draft
                .teams_mut()[team_name]
                .roster_mut()
                .remove(drop_player_name)
                .expect("Player not on team.");
            draft.players.add(&player);

            let player = draft.players
                .by_name(&add_player_name)
                .expect(&format!("Unknown player. {}", add_player_name));
            draft.players.remove(&player);
            draft.teams_mut()[team_name].roster_mut().add(player);
        }
    }
}

#[derive(Clone)]
pub struct Draft {
    players: Players,
    teams: IndexMap<TeamName, Team>,
    team_turns: Vec<(usize, usize)>,
    my_name: TeamName,
    season_matchups: SeasonMatchups,
}

impl Draft {
    pub fn new(
        players: Players,
        teams: IndexMap<TeamName, Team>,
        my_name: TeamName,
        season_matchups: SeasonMatchups,
        round_count: usize) -> Draft {

        let mut team_turns = vec![];
        for round in 1..=round_count {
            for team_index in 0..teams.len() {
                let team_index = if round % 2 == 1 { team_index } else { teams.len() - team_index - 1 };
                team_turns.push((team_index, round));
            }
        }

        Draft{players, teams, team_turns, my_name, season_matchups}
    }

    pub fn advance(
        &mut self,
        drafters: &mut Vec<Box<Drafter>>,
        substitutes: &HashMap<Position, Player>) -> Player {

        let (team_index, _) = self.team_turns[0];
        self.advance_fixed_drafter(&mut *drafters[team_index], substitutes)
    }

    pub fn advance_fixed_drafter(
        &mut self,
        drafter: &mut Drafter,
        substitutes: &HashMap<Position, Player>) -> Player {

        let (team_index, _) = self.team_turns[0];

        let clone_draft = self.clone();
        let player = self.teams
            .get_index_mut(team_index)
            .unwrap()
            .1
            .draft_player(drafter, &mut self.players, substitutes, Some(clone_draft));

        self.team_turns.remove(0);
        player
    }

    pub fn force_pick(&mut self, player: &Player) {
        let (team_index, _) = self.team_turns[0];
        self.teams
            .get_index_mut(team_index)
            .unwrap()
            .1
            .draft_specific_player(&mut self.players, player);
        self.team_turns.remove(0);
    }

    pub fn season_matchups_mut(&mut self) -> &mut SeasonMatchups {
        &mut self.season_matchups
    }

    pub fn is_my_turn(&self) -> bool {
        self.my_name == self.team().name()
    }

    pub fn is_over(&self) -> bool {
        self.team_turns.is_empty()
    }

    pub fn my_name(&self) -> &TeamName {
        &self.my_name
    }

    pub fn team(&self) -> Team {
        self.teams.get_index(self.team_turns[0].0).unwrap().1.clone()
    }

    pub fn round(&self) -> usize {
        self.team_turns[0].1
    }

    pub fn teams(&self) -> &IndexMap<TeamName, Team> {
        &self.teams
    }

    pub fn teams_mut(&mut self) -> &mut IndexMap<TeamName, Team> {
        &mut self.teams
    }

    pub fn remove_player(&mut self, player: &Player) -> Player {
        self.players.remove(&player);
        player.clone()
    }
}

fn substitutes(players: &Players) -> HashMap<Position, Player> {
    let mut substitutes = HashMap::new();
    substitutes.insert(Position::Quarterback, players.by_name(&PlayerName::new("Ryan Tannehill")).expect("Bad sub name!"));
    substitutes.insert(Position::RunningBack, players.by_name(&PlayerName::new("Corey Clement")).expect("Bad sub name!"));
    substitutes.insert(Position::WideReceiver, players.by_name(&PlayerName::new("Allen Hurns")).expect("Bad sub name!"));
    substitutes.insert(Position::TightEnd, players.by_name(&PlayerName::new("Jared Cook")).expect("Bad sub name!"));
    substitutes.insert(Position::Kicker, players.by_name(&PlayerName::new("Daniel Carlson")).expect("Bad sub name!"));
    substitutes.insert(Position::Defense, players.by_name(&PlayerName::new("Detroit")).expect("Bad sub name!"));

    substitutes
}
