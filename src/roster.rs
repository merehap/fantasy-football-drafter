use std::cmp::Ordering;
use std::collections::HashMap;

use indexmap::map::IndexMap;

use lineup_selector::simple_select_lineup;
use player::{Player, PlayerName, Position};
use week::Week;

const MAX_PLAYERS: usize = 19;

#[derive(Clone, Debug)]
pub struct Roster {
    roster: IndexMap<Position, Vec<Player>>,
}

impl Roster {
    pub fn new() -> Roster {
        Roster {
            roster: [
                (Position::Quarterback, vec![]),
                (Position::RunningBack, vec![]),
                (Position::WideReceiver, vec![]),
                (Position::TightEnd, vec![]),
                (Position::Kicker, vec![]),
                (Position::Defense, vec![]),
            ].iter().cloned().collect(),
        }
    }

    pub fn add(&mut self, player: Player) {
        let player_count: usize = self.roster.values()
            .map(|players| players.len())
            .sum();
        assert!(player_count < MAX_PLAYERS, "Can't add more than {} players.", MAX_PLAYERS);

        self.roster.get_mut(&player.position).unwrap().push(player);
    }

    pub fn position_count(&self, position: Position) -> usize {
        self.roster[&position].len()
    }

    pub fn players(&self) -> Vec<Player> {
        let mut result = vec![];
        for ps in self.roster.values() {
            result.append(&mut ps.clone());
        }

        result
    }

    pub fn remove(&mut self, player_name: &PlayerName) -> Option<Player> {
        let mut position = None;
        let mut index = None;
        for (pos, pos_players) in &mut self.roster.iter() {
            position = Some(pos.clone());
            for i in 0..pos_players.len() {
                if &pos_players[i].name == player_name {
                    index = Some(i);
                    break;
                }
            }

            if index.is_some() {
                break;
            }
        }

        match(position, index) {
            (Some(pos), Some(i)) => Some(self.roster.get_mut(&pos).unwrap().remove(i)),
            (_, _) => None,
        }
    }

    pub fn remove_best_by_position<F>(
        &mut self, position: Position, current_week: Week, compare: F) -> Option<Player>
        where F: FnMut(&Player, &Player) -> Ordering {

        self.roster.get_mut(&position).unwrap().sort_by(compare);
        remove_first_active(self.roster.get_mut(&position).unwrap(), current_week)
    }

    // Remove the best player, independent of position (useful for selecting flex).
    // TODO: This assumes all of the player vecs are sorted. The compare
    // function can't be used multiple times since sort_by consumes it.
    pub fn remove_flex<F>(&mut self, current_week: Week, mut compare: F) -> Option<Player>
        where F: FnMut(&Player, &Player) -> Ordering {

        let mut best_players = vec![];
        use player::Position::*;
        for position in &[WideReceiver, RunningBack, TightEnd] {
            if self.roster[position].is_empty() {
                continue;
            }

            // We should sort right before the next line, but see TODO above.
            if best_players.is_empty() ||
                compare(&self.roster[position][0], &best_players[0]) == Ordering::Greater {

                best_players = self.roster[position].clone();
            }
        }

        remove_first_active(&mut best_players, current_week)
    }

    pub fn projected_season_point_total(&self, substitutes: &HashMap<Position, Player>) -> f32 {
        let mut result = 0.0;
        for week in 1..=14 {
            let lineup = simple_select_lineup(&self, &substitutes, Week::new(week));
            result += lineup.projected_week_point_total();
        }

        result
    }
}

fn remove_first_active(players: &mut Vec<Player>, current_week: Week) -> Option<Player> {
    for i in 0..players.len() {
        if players[i].name == PlayerName::new("Devonta Freeman") &&
            (current_week == Week::Two || current_week == Week::Three){
            continue;
        }
        if players[i].bye_week != current_week {
            return Some(players.remove(i))
        }
    }

    None
}
