use std::fmt;

use week::Week;

#[derive(Clone, Debug, PartialEq)]
pub struct Player {
    pub name: PlayerName,
    pub position: Position,
    pub bye_week: Week,
    pub projected_points_p10: f32,
    pub projected_points_p50: f32,
    pub projected_points_p90: f32,
    pub projected_vor: f32,
    pub adp: f32,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct PlayerName(String);

impl PlayerName {
    pub fn new(name: &str) -> PlayerName {
        PlayerName(cleanup_player_name(name))
    }

    pub fn to_raw(&self) -> &str {
        &self.0
    }
}

impl fmt::Display for PlayerName {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        self.0.fmt(formatter)
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum Position {
    Quarterback,
    WideReceiver,
    RunningBack,
    TightEnd,
    Kicker,
    Defense,
}

impl Position {
    pub fn from_abbr(abbr: &str) -> Option<Position> {
        match abbr {
            "QB" => Some(Position::Quarterback),
            "WR" => Some(Position::WideReceiver),
            "RB" => Some(Position::RunningBack),
            "TE" => Some(Position::TightEnd),
            "K"  => Some(Position::Kicker),
            "DST" => Some(Position::Defense),
            _ => None
        }
    }
}

pub fn cleanup_player_name(name: &str) -> String {
    transform_defense_name_to_locale(name)
        .replace("'", "")
        .replace(" Jr.", "")
        .replace(" Sr.", "")
        .replace(" II", "")
        .replace(" IV", "")
        .replace(" V", "")
        .replace(" D/ST", "")
        .replace("Stephen Hauschka", "Steven Hauschka")
}

pub fn transform_defense_name_to_locale(name: &str) -> &str {
    match name {
        "Broncos" => "Denver",
        "Patriots" => "New England",
        "Chiefs" => "Kansas City",
        "Giants" => "New York",
        "Seahawks" => "Seattle",
        "Rams" => "Los Angeles",
        "Bengals" => "Cincinnati",
        "Cardinals" => "Arizona",
        "Panthers" => "Carolina",
        "Vikings" => "Minnesota",
        "Texans" => "Houston",
        "Eagles" => "Philadelphia",
        "Buccaneers" => "Tampa Bay",
        "Steelers" => "Pittsburgh",
        "Falcons" => "Atlanta",
        "Raiders" => "Oakland",
        "Jaguars" => "Jacksonville",
        "Ravens" => "Baltimore",
        "Packers" => "Green Bay",
        "Titans" => "Tennessee",
        "Dolphins" => "Florida",
        "Chargers" => "San Diego",
        "Bills" => "Buffalo",
        "Cowboys" => "Dallas",
        "Redskins" => "Washington",
        "Colts" => "Indianapolis",
        "Saints" => "New Orleans",
        "49ers" => "San Francisco",
        "Lions" => "Detroit",
        "Bears" => "Chicago",
        "Jets" => "New York",
        "Browns" => "Cleveland",
        "Miami" => "Florida",
        _ => name,
    }
}
