use std::fs::read_to_string;
use std::path::PathBuf;

use indexmap::map::IndexMap;

use drafter::*;
use matchup::Matchup;
use season::{Season, SeasonMatchups, WeekMatchups};
use team::{Team, TeamName};
use week::Week;

pub fn load_season_from_rounds(filepath: PathBuf) -> (Season, Vec<Box<Drafter>>){
    let contents = read_to_string(filepath).expect("Season file not found.");
    let sections: Vec<&str> = contents.trim().split("\n\n").collect();
    assert!(sections.len() == 15 || sections.len() == 16,
            "Must be 15 or 16 sections in the season file.");

    let my_name: TeamName = TeamName::new(sections[0].trim());

    let team_names: Vec<TeamName> = sections[1]
        .trim()
        .split('\n')
        .map(TeamName::new)
        .collect();
    assert!(team_names.len() >= 8 && team_names.len() <= 14, format!("Bad number of teams: {}", team_names.len()));
    assert_eq!(team_names.len() % 2, 0, "Team count must be even but was: {}", team_names.len());

    let mut week_matchups = vec![];
    for (i, section) in sections.iter().enumerate().skip(2) {
        let mut raw_week_matchups = section.split('\n');

        if !raw_week_matchups.next()
            .expect("Nothing in this section?")
            .starts_with("Week") {
            panic!("No week header!");
        }

        let mut matchups = vec![];
        for raw_matchup in raw_week_matchups {
            let names: Vec<TeamName> = raw_matchup.split(',')
                .map(|m| TeamName::new(m.trim()))
                .collect();
            matchups.push(Matchup::new(
                names[0].clone(),
                names[1].clone(),
                Week::new((i-1) as u8)));
        }

        let week_matchup = WeekMatchups::new(Week::new((i - 1) as u8), matchups);
        assert_eq!(team_names.len() / 2, week_matchup.week_matchups.len(), "Wrong number of week matchups");
        week_matchups.push(week_matchup);
    }

    let mut teams = IndexMap::new();
    let mut drafters: Vec<Box<Drafter>> = vec![];
    for team_name in team_names {
        teams.insert(team_name.clone(), Team::new(team_name.clone()));
        if team_name == my_name.clone() {
            drafters.push(Box::new(KeyboardDrafter::new(Box::new(MonteCarloDrafter::new()))));
        } else {
            drafters.push(Box::new(KeyboardDrafter::new(Box::new(NaiveVorDrafter::new()))));
        }
    }

    let season = Season::new(
        teams,
        my_name,
        SeasonMatchups::from_week_matchups(&week_matchups));
    (season, drafters)
}
