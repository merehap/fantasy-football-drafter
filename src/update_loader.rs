use std::fs::read_to_string;
use std::path::PathBuf;

use player::PlayerName;
use team::TeamName;
use update::Update;

pub fn load_updates(path: PathBuf) -> Vec<Update> {
    let contents = read_to_string(path).expect("Updates file not found.");
    let raw_updates: Vec<&str> = contents
        .trim()
        .split("\n")
        .map(|u| u.trim())
        .collect();

    let mut updates = vec![];
    for raw_update in raw_updates {
        if raw_update.is_empty() {
            continue;
        }

        let segments: Vec<&str> = raw_update.split('|').collect();
        let update = match segments[0] {
            "Add" => {
                let data: Vec<&str> = segments[1].split(',').collect();
                Update::Add(TeamName::new(data[0]), PlayerName::new(data[1]))
            },
            "Drop" => {
                let data: Vec<&str> = segments[1].split(',').collect();
                Update::Drop(TeamName::new(data[0]), PlayerName::new(data[1]))
            },
            "Waiver" => {
                let data: Vec<&str> = segments[1].split(',').collect();
                Update::Waiver(
                    TeamName::new(data[0]),
                    PlayerName::new(data[1]),
                    PlayerName::new(data[2]))
            },
            "Trade" => {
                let team0_data: Vec<&str> = segments[1].split(',').collect();
                let team1_data: Vec<&str> = segments[2].split(',').collect();
                Update::Trade(
                    TeamName::new(team0_data[0]),
                    vec![PlayerName::new(team0_data[1])],
                    TeamName::new(team1_data[0]),
                    vec![PlayerName::new(team1_data[1])])
            },
            _ => panic!("Unrecognized update: {}", segments[0]),
        };

        updates.insert(0, update);
    }

    updates
}
