use player::Player;

#[derive(Clone)]
pub struct Trade {
    pub team0_offer: Vec<Player>,
    pub team1_offer: Vec<Player>,
}

impl Trade {
    pub fn new(team0_offer: Vec<Player>, team1_offer: Vec<Player>) -> Trade {
        Trade{team0_offer, team1_offer}
    }

    pub fn total_projected_points(&self) -> (f32, f32) {
        (self.team0_offer.iter().map(|p| p.projected_points_p50).sum(),
         self.team1_offer.iter().map(|p| p.projected_points_p50).sum())
    }
}