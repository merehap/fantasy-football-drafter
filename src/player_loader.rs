use std::collections::HashMap;
use std::path::PathBuf;

use csv;

use player;
use player::{Player, PlayerName, Position};
use players::{Players, PlayersBuilder};
use week::Week;

use std::fs::read_to_string;

pub fn load_players_from_csv(filepath: PathBuf, adp_path: Option<PathBuf>) -> Players {
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(filepath)
        .expect("Couldn't open rankings CSV!");
    let mut records = reader.records();
    let headers = records.next()
        .expect("No headers!")
        .expect("Couldn't parse headers!");
    let expected_headers =
        vec!["player", "position", "bye", "lower", "upper", "points", "vor", "adp"];
    let mut header_indices: HashMap<String, usize> = HashMap::new();
    for (i, header) in headers.iter().enumerate() {
        for expected_header in &expected_headers {
            if &header == expected_header {
                header_indices.insert(expected_header.to_string(), i);
            }
        }
    }

    assert!(expected_headers.len() == header_indices.len()
        || expected_headers.len() - 2 == header_indices.len(),
        "Missing headers?");

    let mut adps = None;
    if let Some(path) = adp_path {
        let contents = read_to_string(path).expect("ADP file not found.");
        let lines: Vec<String> = contents.split('\n').map(|l| l.to_string()).collect();

        if !lines[0].starts_with("RANK") {
            panic!("Invalid ADP file (doesn't start with 'RANK')");
        }

        let mut temp_adps = HashMap::new();
        for line in lines.iter().skip(1) {
            let columns: Vec<String> = line.split('\t').map(|l| l.to_string()).collect();
            let name_segments: Vec<String> = columns[1].split(',').map(|c| c.to_string()).collect();
            let player_name = player::cleanup_player_name(&name_segments[0].clone());
            let adp = columns[3].clone();
            temp_adps.insert(player_name, adp);
        }

        adps = Some(temp_adps);
    }

    let mut players_builder = PlayersBuilder::new();

    for record in records {
        let record = record.expect("Not a valid record?");

        let pos_str = &record[header_indices["position"]];
        let bye_str = &record[header_indices["bye"]];
        if let Some(position) = Position::from_abbr(pos_str) {
            let bye_week = bye_str.parse::<u8>().unwrap_or(16);
            let name = &record[header_indices["player"]];
            let maybe_adp = if let Some(val) = header_indices.get("adp") {
                if let Some(rval) = record.get(val.clone()) {
                    rval.parse::<f32>().ok()
                } else {
                    None
                }
            } else if let Some(vals) = adps.clone() {
                if let Some(p) = vals.get(name) {
                    Some(p.parse::<f32>().expect("ADP from adp.txt wasn't a float?"))
                } else {
                    Some(200.0)
                }
            } else {
                None
            };

            if let Some(adp) = maybe_adp {
                let mut projected_points_p10 = record[header_indices["lower"]]
                    .parse::<f32>()
                    .expect("Lower points aren't a float?");
                let mut projected_points_p50 = record[header_indices["points"]]
                    .parse::<f32>()
                    .expect("Points aren't a float?");
                let mut projected_points_p90 = record[header_indices["upper"]]
                    .parse::<f32>()
                    .expect("Upper points aren't a float?");
                let projected_vor = if let Some(index) = header_indices.get("vor") {
                    record[*index]
                        .parse::<f32>()
                        .expect("VOR isn't a float?")
                } else {
                    let low_diff = 14.0 * (projected_points_p50 - projected_points_p10);
                    let high_diff = 14.0 * (projected_points_p90 - projected_points_p50);
                    projected_points_p50 *= 14.0;
                    projected_points_p10 = projected_points_p50 - low_diff;
                    projected_points_p90 = projected_points_p50 + high_diff;
                    0.0
                };

                players_builder.add(Player {
                    name: PlayerName::new(name),
                    position,
                    bye_week: Week::new(bye_week),
                    projected_points_p10,
                    projected_points_p50,
                    projected_points_p90,
                    projected_vor,
                    adp,
                });
            }
        }
    }

    let mut players = players_builder.build();
    if players.players_by_vor().len() < 100 {
        panic!("Only {} players were found.", players.players_by_vor().len());
    }

    if !header_indices.contains_key("vor") {
        set_vors(&mut players);
        let mut builder = PlayersBuilder::new();
        for player in players.players_by_adp() {
            builder.add(player);
        }

        players = builder.build();
    }

    players
}

fn set_vors(players: &mut Players) {

    let mut counts = HashMap::new();
    for player in players.players_by_adp().iter().take(125) {
        let mut count = counts.entry(player.position)
            .or_insert(0);
        *count += 1;
    }

    let mut base_points: HashMap<Position, f32> = HashMap::new();
    use player::Position::*;
    for &position in &[Quarterback, RunningBack, WideReceiver, TightEnd, Kicker, Defense] {
        let pos_players = players.players_by_adp_for_position(position);
        base_points.insert(position,
            (pos_players[counts[&position] - 1].projected_points_p50
                + pos_players[counts[&position]].projected_points_p50
                + pos_players[counts[&position] + 1].projected_points_p50) / 3.0);
    }

    players.foreach(|ref mut player| {
        player.projected_vor = player.projected_points_p50 - base_points[&player.position];
    });
}
