use std::collections::HashMap;

use indexmap::IndexMap;

use lineup_selector::simple_select_lineup;
use player::{Player, Position};
use team::{Team, TeamName};
use week::Week;

#[derive(Clone)]
pub struct Matchup {
    pub left_team_name: TeamName,
    pub right_team_name: TeamName,
    week: Week,
}

impl Matchup {
    pub fn new(left_team_name: TeamName, right_team_name: TeamName, week: Week) -> Matchup {
        Matchup{left_team_name, right_team_name, week}
    }

    pub fn team_win_rate_probability(
        &mut self,
        team_name: &TeamName,
        teams: &IndexMap<TeamName, Team>,
        substitutes: &HashMap<Position, Player>,
        match_count: u32) -> f32 {

        if team_name != &self.left_team_name && team_name != &self.right_team_name {
            panic!("Team name didn't match left nor right.");
        }

        let (this_team, other_team) = if team_name == &self.left_team_name {
            (&teams[&self.left_team_name], &teams[&self.right_team_name])
        } else {
            (&teams[&self.right_team_name], &teams[&self.left_team_name])
        };

        let mut wins = 0;
        for _ in 0..match_count {
            if simple_select_lineup(this_team.roster(), &substitutes, self.week).gen_possible_week_point_total() >
                simple_select_lineup(other_team.roster(), &substitutes, self.week).gen_possible_week_point_total() {

                wins += 1;
            }
        }

        (wins as f32) / (match_count as f32) }
}