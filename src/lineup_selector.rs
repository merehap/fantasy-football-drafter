use std::cmp::Ordering;
use std::collections::HashMap;

use player::{Player, Position};
use roster::Roster;
use lineup::{Lineup, LineupBuilder};
use week::Week;

pub fn simple_select_lineup(
    roster: &Roster,
    substitutes: &HashMap<Position, Player>,
    current_week: Week) -> Lineup {

    let mut roster = roster.clone();
    let best_by_projected_points = |p0: &Player, p1: &Player| {
        if p0.projected_points_p50 < p1.projected_points_p50 {
            Ordering::Greater
        } else if p0.projected_points_p50 > p1.projected_points_p50 {
            Ordering::Less
        } else {
            Ordering::Equal
        }
    };

    let mut builder = LineupBuilder::new(current_week);
    if let Some(qb) = roster.remove_best_by_position(Position::Quarterback, current_week, best_by_projected_points) {
        builder.add_positioned_player(qb);
    }
    if let Some(te) = roster.remove_best_by_position(Position::TightEnd, current_week, best_by_projected_points) {
        builder.add_positioned_player(te);
    }
    if let Some(kicker) = roster.remove_best_by_position(Position::Kicker, current_week, best_by_projected_points) {
        builder.add_positioned_player(kicker);
    }
    if let Some(def) = roster.remove_best_by_position(Position::Defense, current_week, best_by_projected_points) {
        builder.add_positioned_player(def);
    }
    if let Some(wr) = roster.remove_best_by_position(Position::WideReceiver, current_week, best_by_projected_points) {
        builder.add_positioned_player(wr);
    }
    if let Some(wr) = roster.remove_best_by_position(Position::WideReceiver, current_week, best_by_projected_points) {
        builder.add_positioned_player(wr);
    }
    if let Some(rb) = roster.remove_best_by_position(Position::RunningBack, current_week, best_by_projected_points) {
        builder.add_positioned_player(rb);
    }
    if let Some(rb) = roster.remove_best_by_position(Position::RunningBack, current_week, best_by_projected_points) {
        builder.add_positioned_player(rb);
    }
    if let Some(flex) = roster.remove_flex(current_week, best_by_projected_points) {
        builder.add_flex_player(flex);
    }

    if builder.quarterback.is_none() {
        builder.quarterback = Some(substitutes[&Position::Quarterback].clone());
    }

    if builder.running_backs.is_empty() {
        builder.running_backs.push(substitutes[&Position::RunningBack].clone());
    }

    if builder.wide_receivers.is_empty() {
        builder.wide_receivers.push(substitutes[&Position::WideReceiver].clone());
    }

    if builder.tight_end.is_none() {
        builder.tight_end = Some(substitutes[&Position::TightEnd].clone());
    }

    if builder.kicker.is_none() {
        builder.kicker = Some(substitutes[&Position::Kicker].clone());
    }

    if builder.defense.is_none() {
        builder.defense = Some(substitutes[&Position::Defense].clone());
    }

    builder.build()
}
