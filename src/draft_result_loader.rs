use std::collections::VecDeque;
use std::fs::read_to_string;
use std::path::PathBuf;

use drafter::{Drafter, PredeterminedDrafter, NaiveVorDrafter};
use player;
use player::{Player, PlayerName};
use players::Players;
use team::TeamName;

use indexmap::map::IndexMap;

pub fn load_results_from_rounds(filepath: PathBuf, players: &Players) -> Vec<Box<Drafter>> {

    let contents = read_to_string(filepath).expect("Draft results file not found.");
    let is_espn = contents.contains(',');
    let mut lines: Vec<&str> = contents.split('\n').collect();

    if !lines[0].starts_with("Round 1") {
        panic!("Invalid draft results file (doesn't start with 'Round 1')");
    }

    if is_espn {
        lines = lines.into_iter()
            .filter(|line| !line.contains(','))
            .collect::<Vec<_>>();
    }



    let mut players_by_team_name = IndexMap::new();
    for line in &lines {
        if line.starts_with("Round 2") {
            break;
        }

        if line.starts_with("Round") {
            continue;
        }

        let drafter_name = if is_espn {
            TeamName::new(line.split('\t').collect::<Vec<_>>()[1])
        } else {
            TeamName::new(line.split('\t').last().expect("Blank line?"))
        };

        players_by_team_name.insert(drafter_name, VecDeque::new());
    }

    for line in lines {
        if line.starts_with("Round") || line.is_empty() {
            continue;
        }

        let (drafter_name, player_name): (TeamName, &str) = if is_espn {
            (TeamName::new(line.split('\t').collect::<Vec<_>>()[1]),
            line.split('\t').collect::<Vec<_>>()[2])
        } else {
            (TeamName::new(line.split('\t').last().expect("Blank line?")),
            line.split('\t')
                .collect::<Vec<_>>()[1]
                .split('(')
                .collect::<Vec<_>>()[0])
        };

        players_by_team_name.get_mut(&drafter_name)
            .expect("Unknown drafter in results.")
            .push_back(player_name.trim().to_string());
    }

    let mut drafters: Vec<Box<Drafter>> = vec![];
    for (_, player_names) in players_by_team_name {
        let mut draft_picks: VecDeque<Player> = VecDeque::new();
        for player_name in player_names {
            let player_name = player::cleanup_player_name(&player_name).clone();
            draft_picks.push_back(
                players.by_name(&PlayerName::new(&player_name))
                .unwrap_or_else(|| panic!("Player name mismatch: {}", &player_name)));
        }

        let drafter = PredeterminedDrafter::new(draft_picks, Box::new(NaiveVorDrafter::new()));
        drafters.push(Box::new(drafter));
    }

    drafters
}