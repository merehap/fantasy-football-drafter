use std::borrow::Borrow;
use std::collections::HashMap;

use draft::Draft;
use drafter::Drafter;
use player::{Player, Position};
use players::Players;
use roster::Roster;

#[derive(Clone, Debug, Eq, PartialEq, Hash)]
pub struct TeamName(String);

impl TeamName {
    pub fn new(string: &str) -> TeamName {
        TeamName(string.to_string())
    }

    pub fn to_raw(&self) -> &str {
        &self.0
    }
}

impl Into<String> for TeamName {
    fn into(self) -> String {
        self.0
    }
}

impl Borrow<String> for TeamName {
    fn borrow(&self) -> &String {
        &self.0
    }
}

#[derive(Clone)]
pub struct Team {
    name: TeamName,
    roster: Roster,
}

impl Team {
    pub fn new(name: TeamName) -> Team {
        Team{ name, roster: Roster::new()}
    }

    pub fn draft_player(
        &mut self,
        drafter: &mut Drafter,
        players: &mut Players,
        substitutes: &HashMap<Position, Player>,
        draft: Option<Draft>) -> Player {

        let player = drafter.pick_player(players, &self.roster, substitutes, draft);
        self.draft_specific_player(players, &player);
        player
    }

    pub fn draft_specific_player(&mut self, players: &mut Players, player: &Player) {
        self.roster.add(player.clone());
        players.remove(player);
    }

    pub fn name(&self) -> TeamName {
        self.name.clone()
    }

    pub fn desc(&self) -> String {
        format!("{:?}", self.name()).to_string()
    }

    pub fn roster(&self) -> &Roster {
        &self.roster
    }

    pub fn roster_mut(&mut self) -> &mut Roster {
        &mut self.roster
    }
}
