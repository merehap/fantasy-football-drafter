use std::cmp::Ordering;
use std::collections::HashMap;
use indexmap::map::IndexMap;

use player::{Player, PlayerName, Position};

#[derive(Clone)]
pub struct Players {
    best_by_points: HashMap<Position, IndexMap<PlayerName, Option<Player>>>,
    best_by_vor: HashMap<Position, IndexMap<PlayerName, Option<Player>>>,
    best_by_adp: HashMap<Position, IndexMap<PlayerName, Option<Player>>>,
}

impl Players {
    pub fn players_by_vor_for_position(&self, position: Position) -> Vec<Player> {
        self.best_by_vor[&position].values()
            .filter_map(|v| v.clone())
            .collect::<Vec<_>>()
    }

    pub fn players_by_adp_for_position(&self, position: Position) -> Vec<Player> {
        self.best_by_adp[&position].values()
            .filter_map(|v| v.clone())
            .collect::<Vec<_>>()
    }

    pub fn by_name(&self, name: &PlayerName) -> Option<Player> {
        for position_players in self.best_by_points.values() {
            if position_players.contains_key(name) {
                return Some(position_players[name].clone())
                    .expect("Why was there a None?");
            }
        }

        None
    }

    pub fn add(&mut self, player: &Player) {
        self.best_by_points.get_mut(&player.position).unwrap()[&player.name] = Some(player.clone());
        self.best_by_vor.get_mut(&player.position).unwrap()[&player.name] = Some(player.clone());
        self.best_by_adp.get_mut(&player.position).unwrap()[&player.name] = Some(player.clone());
    }

    pub fn remove(&mut self, player: &Player) {
        self.best_by_points.get_mut(&player.position).unwrap()[&player.name] = None;
        self.best_by_vor.get_mut(&player.position).unwrap()[&player.name] = None;
        self.best_by_adp.get_mut(&player.position).unwrap()[&player.name] = None;
    }

    pub fn players_by_vor(&self) -> Vec<Player> {
        let mut players: Vec<Player> = vec![];
        for ps in self.best_by_vor.values() {
            let mut psc = ps.values()
                .filter_map(|v| v.clone())
                .collect::<Vec<_>>();
            players.append(&mut psc);
        }

        let best_by_vor = |p0: &Player, p1: &Player| {
            if p0.projected_vor < p1.projected_vor {
                Ordering::Greater
            } else {
                Ordering::Less
            }
        };

        players.sort_by(best_by_vor);
        players
    }

    pub fn players_by_adp(&self) -> Vec<Player> {
        let mut players: Vec<Player> = vec![];
        for ps in self.best_by_adp.values() {
            let mut psc = ps.values()
                .filter_map(|v| v.clone())
                .collect::<Vec<_>>();
            players.append(&mut psc);
        }

        let best_by_adp = |p0: &Player, p1: &Player| {
            if p0.adp > p1.adp {
                Ordering::Greater
            } else {
                Ordering::Less
            }
        };

        players.sort_by(best_by_adp);
        players
    }

    pub fn foreach<F>(&mut self, func: F)
        where F: Fn(&mut Player) {
        for player in self.players_by_vor() {
            for mut player in &mut self.best_by_points
                .get_mut(&player.position)
                .unwrap()[&player.name] {

                func(&mut player);
            }

            for player in &mut self.best_by_vor
                .get_mut(&player.position)
                .unwrap()[&player.name] {

                func(player);
            }

            for player in &mut self.best_by_adp
                .get_mut(&player.position)
                .unwrap()[&player.name] {

                func(player);
            }
        }
    }
}

pub struct PlayersBuilder {
    players: Vec<Player>,
}

impl PlayersBuilder {
    pub fn new() -> PlayersBuilder {
        PlayersBuilder{players: vec![]}
    }

    pub fn add(&mut self, player: Player) {
        self.players.push(player);
    }

    pub fn build(&self) -> Players {
        let mut players_by_points = self.players.clone();
        let mut players_by_vor = self.players.clone();
        let mut players_by_adp = self.players.clone();

        let best_by_points = |p0: &Player, p1: &Player| {
            if p0.projected_points_p50 < p1.projected_points_p50 {
                Ordering::Greater
            } else if p0.projected_points_p50 > p1.projected_points_p50 {
                Ordering::Less
            } else {
                Ordering::Equal
            }
        };
        players_by_points.sort_by(best_by_points);

        let best_by_vor = |p0: &Player, p1: &Player| {
            if p0.projected_vor < p1.projected_vor {
                Ordering::Greater
            } else if p0.projected_vor > p1.projected_vor {
                Ordering::Less
            } else {
                Ordering::Equal
            }
        };
        players_by_vor.sort_by(best_by_vor);

        let best_by_adp = |p0: &Player, p1: &Player| {
            if p0.adp < p1.adp{
                Ordering::Less
            } else if p0.adp > p1.adp {
                Ordering::Greater
            } else {
                Ordering::Equal
            }
        };
        players_by_adp.sort_by(best_by_adp);

        Players {
            best_by_points: sorted_vec_to_nested_map(players_by_points),
            best_by_vor: sorted_vec_to_nested_map(players_by_vor),
            best_by_adp: sorted_vec_to_nested_map(players_by_adp),
        }
    }
}

fn sorted_vec_to_nested_map(vec: Vec<Player>) -> HashMap<Position, IndexMap<PlayerName, Option<Player>>> {
    let mut result = HashMap::new();
    use player::Position::*;
    for &position in &[Quarterback, WideReceiver, RunningBack, TightEnd, Kicker, Defense] {
        result.insert(position, IndexMap::new());
    }

    for player in vec {
        let mut duplicate = false;
        for position in &[Quarterback, WideReceiver, RunningBack, TightEnd, Kicker, Defense] {
            if result[position].contains_key(&player.name) {
                println!("Duplicate player name '{}'. Ignoring the newer player.", player.name.clone());
                duplicate = true;
                break;
            }
        }

        if duplicate {
            continue;
        }

        result.get_mut(&player.position)
            .expect("Position missing!")
            .insert(player.name.clone(), Some(player.clone()));
    }

    result
}